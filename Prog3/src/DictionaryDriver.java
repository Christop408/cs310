/*  DictionaryDriver
    Driver class to test your LatinDictionary application.
    CS310 Spring 2017
    Alan Riggins
*/

import java.util.Iterator;

import data_structures.*;

public class DictionaryDriver {
    public static void main(String [] args) {
        DictionaryADT< Integer , Integer > adt =
                //new BinarySearchTree<>();
                new HashTable<>(15);
                //new OrderedArrayDictionary<>(15);
                //new RedBlackTree<>();

        runTests(adt);
        riggins();

        adt.add(5,5);
        adt.add(3,3);
        adt.add(2,2);
        adt.add(1,1);
        adt.add(8,8);
        adt.add(7,7);
        adt.add(6,6);
        adt.add(10,10);

        System.out.println(adt.delete(5));
        print(adt);

        System.out.println("\n\nDictionaryTester...\n");

        DictionaryTester.main(args);

    }

    private static void riggins() {
        System.out.println("---------------------------------------------------------------------\n" +
                                            "\t\t\t\t\t\t\tRiggins\n" +
                           "---------------------------------------------------------------------\n");
        new DictionaryDriver();
    }

    public static void runTests(DictionaryADT adt) {
        load(adt); //Load dummy data
        info(adt);
        addToFull(adt);
        getKV(adt);
        randomDeletes(adt, 5); //Delete 5 random elements (only works for ints)
        deleteFirst(adt);
        print(adt); // Print contents - tests iterators
        contains(adt);
        adds(adt);
        info(adt);
        adt.clear();
        info(adt);
        getKV(adt);
    }

    private static void getKV(DictionaryADT adt) {
        System.out.println("Value for k=3::" + adt.getValue(3));
        System.out.println("Key for string=3::" + adt.getKey("string=3"));
        System.out.println("Value for k=44::" + adt.getValue(44));
        System.out.println("Key for string=44::" + adt.getKey("string=44"));
        System.out.println();
    }

    private static void addToFull(DictionaryADT adt) {
        if(adt.isFull()) System.out.println("List is full. Adding: " + adt.add(500, "five hundred"));
        else System.out.println("List is not full.");
        System.out.println();
    }

    private static void info(DictionaryADT adt) {
        System.out.println("Size: " + adt.size());
        System.out.println("Empty? " + adt.isEmpty() + ", full? " + adt.isFull());
        System.out.println();
    }

    private static void adds(DictionaryADT adt) {
        System.out.println("Adding 40 the first time: " + adt.add(40, "forty"));
        System.out.println("Adding 40 the second time: " + adt.add(40, "forty"));
        System.out.println("Removing 40: " + adt.delete(40) + ", Then adding it again: " + adt.add(40, "forty"));
        System.out.println();
    }

    private static void contains(DictionaryADT adt) {
        Iterator keys = adt.keys();
        boolean passed = true;
        while(keys.hasNext()) {
            Integer key = (Integer) keys.next();
            if(!adt.contains(key)) {
                passed = false;
                break;
            }
        }
        if(passed) System.out.println("Contains all keys.");
        else System.out.println("Something was wrong...");

        if(adt.contains(300)) System.out.println("Contains 300... That shouldn't happen.");
        else System.out.println("Does not contain 300. Check!");
        System.out.println();
    }

    public static void deleteFirst(DictionaryADT adt) {
        adt.add(-1,"string=-1");
        adt.add(-2,"string=-2");
        adt.delete(0);
        System.out.println("Value for first element (0) = " + adt.getValue(0));
        System.out.println();
    }

    private static void randomDeletes(DictionaryADT adt, int deletes) {
        System.out.println("Size before delete: " + adt.size());
        if(deletes > adt.size()) throw new IllegalArgumentException("deletes can't be bigger than size()");
        for(int i = 0 ; i < deletes ; i++) {
            int index = (int) (Math.random() * adt.size());
            System.out.println(index);
            adt.delete(index);
        }
        System.out.println("Size after deletes: " + adt.size());
        System.out.println();
    }

    static void load(DictionaryADT adt) {
        for(Integer i = 0; i < 15 ; i++) {
            adt.add(i, "string=" + i);
        }
        System.out.println("Loaded Map. \n");
    }

    static void print(DictionaryADT adt) {
        //Print
        Iterator keys = adt.keys();
        Iterator vals = adt.values();

        while(keys.hasNext()) {
            System.out.println(keys.next() + "(k): " + vals.next() + "(v)");
        }
        System.out.println();
    }

    public DictionaryDriver() {
        String [] words = {
                "vobis","castanea","agricola","pubes","consilium","atavus","vulgus",
                "iuglans"};
        LatinDictionary dictionary = new LatinDictionary();
        dictionary.loadDictionary("Latin.txt");
        String definition;

        for(int i=0; i < words.length; i++) {
            definition = dictionary.getDefinition(words[i]);
            if(definition == null)
                System.out.println(
                        "Sorry, " + words[i] + " was not found.\n");
            else
                System.out.println(
                        "The definition of " + words[i] + " is:\n" +
                                definition + ".\n");
        }

        // add a word not in the data file and make sure it can be found.
        dictionary.insertWord("iuglans","A walnut.  Either the nut or the tree");
        definition = dictionary.getDefinition("iuglans");
        if(definition == null)
            System.out.println(
                    "Sorry, iuglans" + " was not found.\n");
        else
            System.out.println(
                    "The definition of iuglans" + " is:\n" +
                            definition + ".\n");

        if(!dictionary.deleteWord(words[0]))
            System.out.println("ERROR, delete FAILED!!!");
        if(dictionary.getDefinition(words[0]) != null)
            System.out.println("ERROR, returned deleted definition.");

        System.out.println("Now checking the getRange method\n");
        String [] myWords = dictionary.getRange("a","c");
        for(int i=0; i < myWords.length; i++)
            System.out.println(myWords[i] + "=" + dictionary.getDefinition(myWords[i]));
        System.out.println("Total size: " + myWords.length);
    }
}