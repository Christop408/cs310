package data_structures;

import java.util.ConcurrentModificationException;
import java.util.Iterator;

/**
 * Created by chrisgilardi on 4/5/17.
 */

public class OrderedArrayDictionary<K extends Comparable<K>, V> implements DictionaryADT<K, V> {

    DictionaryNode<K, V>[] underArray;
    int currentSize;
    int modCounter;

    public OrderedArrayDictionary(int initialSize) {
        underArray = new DictionaryNode[initialSize];
        currentSize = 0;
        modCounter = 0;
    }

    public boolean contains(K key) {
        if(isEmpty()) return false;
        return binSearch(new DictionaryNode<>(key, null),0,currentSize - 1) >= 0;
    }

    public boolean add(K key, V value) {
        if(isFull()) return false;
        if(isEmpty()) {
            underArray[0] = new DictionaryNode<>(key, value);
        } else {
            if(contains(key)) return false;
            int insertIndex = insertSearch(new DictionaryNode<>(key, null), 0, currentSize - 1);
            for(int i = currentSize; i > insertIndex ; i--) {
                underArray[i] = underArray[i-1];
            }
            underArray[insertIndex] = new DictionaryNode<>(key, value);
        }
        currentSize++;
        modCounter++;
        return true;
    }

    public boolean delete(K key) {
        if(isEmpty()) return false;
        int index = binSearch(new DictionaryNode<>(key, null),0, currentSize - 1);
        if(index == -1) return false;
        for(int i = index ; i < currentSize - 1 ; i++) {
            underArray[i] = underArray[i + 1];
        }
        currentSize--;
        modCounter++;
        return true;
    }

    public V getValue(K key) {
        int index = binSearch(new DictionaryNode<>(key, null), 0, currentSize - 1);
        if(index != -1) return underArray[index].value;
        return null;
    }

    public K getKey(V value) {
        for(int i = 0 ; i < currentSize ; i++) {
            if(((Comparable<V>) underArray[i].value).compareTo(value) == 0) {
                return underArray[i].key;
            }
        }
        return null;
    }

    public int size() {
        return currentSize;
    }

    public boolean isFull() {
        return currentSize == underArray.length;
    }

    public boolean isEmpty() {
        return currentSize == 0;
    }

    public void clear() {
        currentSize = 0;
        modCounter = 0;
    }

    private int binSearch(DictionaryNode<K, V> obj, int lo, int hi) {
        if(hi < lo) return -1;
        int mid = (lo + hi) >> 1;
        int status = obj.key.compareTo(underArray[mid].key);
        if(status == 0) return mid;
        if(status < 0) return binSearch(obj, lo, mid - 1);
        return binSearch(obj, mid + 1, hi);
    }

    private int insertSearch(DictionaryNode<K, V> obj, int lo, int hi) {
        if(hi < lo) return lo;
        int mid = (lo+hi) >> 1;
        int status = obj.key.compareTo(underArray[mid].key);
        if(status < 0) return insertSearch(obj, lo, mid-1);
        return insertSearch(obj, mid+1, hi);
    }

    public Iterator<K> keys() {
        return new IteratorHelper<K>() {
            public K next() {
                return nextNode().key;
            }
        };
    }

    public Iterator<V> values() {
        return new IteratorHelper<V>() {
            public V next() {
                return nextNode().value;
            }
        };
    }

    abstract class IteratorHelper<T> implements Iterator<T> {

        int iterIndex;
        long mCounter;

        public IteratorHelper() {
            iterIndex = currentSize - 1;
            mCounter = modCounter;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }

        public boolean hasNext() {
            return !(iterIndex < 0);
        }

        public abstract T next();

        protected DictionaryNode<K, V> nextNode() {
            if(mCounter != modCounter) throw new ConcurrentModificationException();
            return underArray[iterIndex--];
        }
    }

    class DictionaryNode<K extends Comparable<K>, V> implements Comparable<DictionaryNode<K, V>> {

        public K key;
        public V value;

        public DictionaryNode(K k, V v) {
            this.key = k;
            this.value = v;
        }

        public int compareTo(DictionaryNode<K, V> o) {
            return key.compareTo(o.key);
        }
    }
}


