package data_structures;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.Objects;

/**
 * Created by chrisgilardi on 4/5/17.
 */
public class BinarySearchTree<K extends Comparable<K>, V> implements DictionaryADT<K, V> {

    TreeNode<K, V> root;
    int currentSize;
    long modCounter;

    TreeNode<K , V> foundNode;

    public BinarySearchTree() {
        currentSize = 0;
        modCounter = 0;
        root = null;
    }

    public boolean contains(K key) {
        if(currentSize == 0) return false;
        else return findValue(key, root) != null;
    }

    public boolean add(K key, V value) {
        if(contains(key)) return false;
        if(currentSize == 0)
            root = new TreeNode<>(key, value);
        else
            insert(key, value, root, null, false);
        currentSize++;
        modCounter++;
        return true;
    }

    public boolean delete(K key) {
        if(isEmpty())
            return false;
        if(!contains(key))
            return false;
        root = handleDelete(key,root,null,false);
        currentSize--;
        modCounter++;
        return true;
    }

    private TreeNode<K,V> handleDelete(K k,TreeNode<K,V> n,TreeNode<K,V> parent,boolean wasLeft) {
        if(n == null)
            return null;
        else if(k.compareTo(n.key) < 0)
            handleDelete(k,n.left,n,true);
        else if(k.compareTo(n.key) > 0)
            handleDelete(k,n.right,n,false);
        else {
            if(n.left == null && n.right == null) {
                if(root.key.compareTo(n.key) == 0)
                    n = null;
                else if(wasLeft)
                    parent.right = null;
                else
                    parent.right = null;
            }
            else if(n.left == null) {
                if(root.key.compareTo(n.key) == 0)
                    n = n.right;
                else if(wasLeft)
                    parent.right = n.right;
                else
                    parent.right = n.right;
            }
            else if(n.right == null) {
                if(root.key.compareTo(n.key) == 0)
                    n = n.left;
                else if(wasLeft)
                    parent.left = n.left;
                else
                    parent.right = n.left;
            }
            else {
                TreeNode<K,V> tmp = findIOSuccessorAndDelete(n.right, null);
                handleDelete(tmp.key,n.right,n,false);
                n.key = tmp.key;
                n.value = tmp.value;
            }
        }
        return n;
    }

    public V getValue(K key) {
        if(currentSize == 0) return null;
        return findValue(key, root);
    }

    public K getKey(V value) {
        findKey(value, root);
        if(foundNode != null) {
            K temp = foundNode.key;
            foundNode = null;
            return temp;
        }
        return null;
    }

    private void findKey(V val, TreeNode<K, V> n) {
        if(n != null) {
            if(((Comparable<V>) n.value).compareTo(val) == 0) {
                foundNode = n;
                return;
            }
            if(foundNode != null) return;
            findKey(val, n.left);
            if(foundNode != null) return;
            findKey(val, n.right);
        }
    }

    public int size() {
        return currentSize;
    }

    public boolean isFull() {
        return false;
    }

    public boolean isEmpty() {
        return currentSize == 0;
    }

    public void clear() {
        root = null;
        currentSize = 0;
        modCounter = 0;
    }

    private V findValue(K key, TreeNode<K, V> n) {
        if(n == null) return null;
        if(key.compareTo(n.key) < 0)
            return findValue(key, n.left);
        if(key.compareTo(n.key) > 0)
            return findValue(key, n.right);
        return n.value;
    }

    private void insert(K k, V v, TreeNode<K,V> n, TreeNode<K,V> parent, boolean wasLeft) {
        if(n == null){
            if(wasLeft) parent.left = new TreeNode<>(k, v);
            else parent.right = new TreeNode<>(k,v);
        }
        else if(k.compareTo(n.key) < 0) insert(k, v, n.left, n, true);
        else insert(k, v, n.right, n, false);
    }

    private TreeNode<K, V> findIOSuccessorAndDelete(TreeNode<K , V> n, TreeNode<K, V> parent) {
        if(n.left == null) {
            TreeNode<K, V> temp = n;
            if(parent != null) parent.left = null;
            return temp;
        }
        else return findIOSuccessorAndDelete(n.left, n);
    }


    public Iterator<K> keys() {
        return new BSTIterator<K>() {
            public K next() {
                if(hasNext())
                    return nextNode().key;
                return null;
            }
        };
    }

    public Iterator<V> values() {
        return new BSTIterator<V>() {
            public V next() {
                if(hasNext())
                    return nextNode().value;
                return null;
            }
        };
    }

    abstract class BSTIterator<D> implements Iterator<D> {

        long mCount = modCounter;
        TreeNode<K ,V>[] arrRep;
        int index = 0;
        int loadIndex = 0;

        public BSTIterator() {
            arrRep = new TreeNode[currentSize];
            loadArray(root);
        }

        private void loadArray(TreeNode<K,V> n) {
            if(n != null) {
                loadArray(n.left);
                arrRep[loadIndex++] = n;
                loadArray(n.right);
            }
        }

        public boolean hasNext() {
            if(mCount != modCounter) throw new ConcurrentModificationException();
            return index != currentSize;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }

        protected TreeNode<K , V> nextNode() {
            return arrRep[index++];
        }

    }
}

class TreeNode<T extends Comparable<T>, D> implements Comparable<TreeNode<T,D>> {
    TreeNode<T, D> left;
    TreeNode<T, D> right;
    T key;
    D value;

    public TreeNode(T t, D d) {
        this.key = t;
        this.value = d;
        left = null;
        right = null;
    }

    @Override
    public int compareTo(TreeNode<T, D> o) {
        return this.key.compareTo(o.key);
    }
}
