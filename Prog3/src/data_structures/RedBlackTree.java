package data_structures;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.TreeMap;

/**
 * Created by chrisgilardi on 4/5/17.
 */

//Use TreeMap
public class RedBlackTree<K extends Comparable<K>, V> implements DictionaryADT<K, V> {

    TreeMap<K, V> map;
    private int currentSize;
    private long modCounter;

    public RedBlackTree() {
        map = new TreeMap<>();
        currentSize = 0;
        modCounter = 0;
    }

    public boolean contains(K key) {
        return map.containsKey(key);
    }

    public boolean add(K key, V value) {
        if(contains(key)) return false;
        map.put(key, value);
        currentSize++;
        modCounter++;
        return true;
    }

    public boolean delete(K key) {
        if (map.remove(key) != null) {
            currentSize--;
            modCounter++;
            return true;
        }
        return false;
    }

    public V getValue(K key) {
        return map.get(key);
    }

    public K getKey(V value) {
        for(K key : map.keySet()) {
            if(((Comparable<V>) map.get(key)).compareTo(value) == 0) {
                return key;
            }
        }
        return null;
    }

    public int size() {
        return currentSize;
    }

    public boolean isFull() {
        return false;
    }

    public boolean isEmpty() {
        return currentSize == 0;
    }

    public void clear() {
        map.clear();
        currentSize = 0;
        modCounter = 0;
    }

    public Iterator<K> keys() {
        return new KeyIterator();
    }

    public Iterator<V> values() {
        return new ValueIterator();
    }

    class KeyIterator extends RBTIterator<K> {

        K[] keys;

        public KeyIterator() {
            keys = (K[]) new Comparable[currentSize];
            for(K key : map.keySet())
                keys[loadIndex++] = key;
        }

        public K next() {
            if(mCount != modCounter) throw new ConcurrentModificationException();
            return keys[iterIndex++];
        }

    }

    class ValueIterator extends RBTIterator<V> {

        V[] values;

        public ValueIterator() {
            values = (V[]) new Object[currentSize];
            for(V val : map.values()) {
                values[loadIndex++] = val;
            }
        }

        public V next() {
            if(mCount != modCounter) throw new ConcurrentModificationException();
            return values[iterIndex++];
        }
    }

    abstract class RBTIterator<X> implements Iterator<X> {
        int loadIndex;
        int iterIndex;
        long mCount;

        public RBTIterator() {
            loadIndex = 0;
            iterIndex = 0;
            mCount = modCounter;
        }

        public boolean hasNext() {
            return iterIndex != currentSize;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
