package data_structures;

import java.util.ConcurrentModificationException;
import java.util.Iterator;

/**
 * Created by chrisgilardi on 4/5/17.
 */
public class HashTable<K extends Comparable<K>, V> implements DictionaryADT<K, V> {

    UnorderedList<DictionaryNode<K, V>>[] list;
    private int currentSize;
    private int maxSize;
    private long modCounter;

    public HashTable(int initialSize) {
        list = new UnorderedList[(int) (initialSize * 1.3)];

        for(int i = 0 ; i < list.length ; i++) {
            list[i] = new UnorderedList<>();
        }
        maxSize = initialSize;
        currentSize = 0;
        modCounter = 0;
    }

    public boolean contains(K key) {
        return find(key) != null;
    }

    public DictionaryNode<K, V> find(K key) {
        int index = getIndex(key);
        return list[index].find(new DictionaryNode<>(key, null));
    }

    public boolean add(K key, V value) {
        if(!contains(key)) {
            if(!isFull()) {
                list[getIndex(key)].add(new DictionaryNode<>(key, value));
                currentSize++;
                modCounter++;
                return true;
            }
        }
        return false;
    }

    public boolean delete(K key) {
        int index = getIndex(key);
        if(list[index].remove(new DictionaryNode<>(key, null)) != null) {
            currentSize--;
            modCounter++;
            return true;
        }
        return false;
    }

    public V getValue(K key) {
        int index = getIndex(key);
        DictionaryNode<K, V> node = list[index].find(new DictionaryNode<>(key, null));
        if(node != null) return node.value;
        return null;
    }

    public K getKey(V value) {
        for(int i = 0 ; i < list.length ; i++) {
            for(DictionaryNode<K, V> node : list[i]) {
                if(node != null && (((Comparable<V>) node.value).compareTo(value) == 0)) {
                    return node.key;
                }
            }
        }
        return null;
    }

    public int size() {
        return currentSize;
    }

    public boolean isFull() {
        return currentSize == maxSize;
    }

    public boolean isEmpty() {
        return currentSize == 0;
    }

    public void clear() {
        for(int i = 0 ; i < list.length ; i++) {
            list[i].clear();
        }
        currentSize = 0;
        modCounter = 0;
    }

    private int getIndex(K key) {
        return Math.abs(key.hashCode() % list.length);
    }

    public Iterator<K> keys() {
        return new IteratorHelper<K>() {
            public K next() {
                DictionaryNode<K, V> node = nextNode();
                if(node == null) return null;
                return node.key;
            }
        };
    }

    public Iterator<V> values() {
        return new IteratorHelper<V>() {
            public V next() {
                DictionaryNode<K, V> node = nextNode();
                if(node == null) return null;
                return node.value;
            }
        };
    }


    abstract class IteratorHelper<T> implements Iterator<T> {

        DictionaryNode<K,V>[] array;
        int iterIndex;
        long mCount;

        public IteratorHelper() {
            int index = 0;
            iterIndex = 0;
            mCount = modCounter;
            array = new DictionaryNode[currentSize];
            for(UnorderedList<DictionaryNode<K,V>> miniList : list) {
                for(DictionaryNode<K, V> node : miniList) {
                    array[index++] = node;
                }
            }
            sort();
        }

        public boolean hasNext() {
            //System.out.println(iterIndex);
            return iterIndex != currentSize;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }

        public abstract T next();

        protected DictionaryNode<K, V> nextNode() {
            if(mCount != modCounter) throw new ConcurrentModificationException("Tainted Iterator!");
            return array[iterIndex++];
        }

        private void sort() {
            DictionaryNode<K,V> temp;
            int gap = 1;
            while (gap <= (array.length / 3))
                gap = gap * 3 + 1;
            int inner;
            int outer;
            while (gap > 0) {
                for (outer = gap; outer < array.length; outer++) {
                    temp = array[outer];
                    inner = outer;
                    while (inner > gap - 1 && array[inner - gap].compareTo(temp) >= 0) {
                        array[inner] = array[inner - gap];
                        inner -= gap;
                    }
                    array[inner] = temp;
                }
                gap = (gap - 1) / 3;
            }
        }
    }

    class DictionaryNode<K extends Comparable<K>, V> implements Comparable<DictionaryNode<K, V>> {

        public K key;
        public V value;

        public DictionaryNode(K k, V v) {
            this.key = k;
            this.value = v;
        }

        public int compareTo(DictionaryNode<K, V> o) {
            return key.compareTo(o.key);
        }
    }

}
