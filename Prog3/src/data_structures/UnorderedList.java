package data_structures;

import java.util.ConcurrentModificationException;
import java.util.Iterator;

/**
 * Created by Chris on 4/10/2017.
 */
public class UnorderedList<E extends Comparable<E>> implements Iterable<E> {

    private Node<E> head, tail;
    private int currentSize;
    private long modCounter;

    public UnorderedList() {
        head = tail = null;
        currentSize = 0;
        modCounter = 0;
    }

    public boolean contains(E obj) {
        return this.find(obj) != null;
    }

    public E remove(E obj) {
        if(head == null) return null;
        if(currentSize == 1)
            if(head.data.compareTo(obj) == 0) {
                E toReturn = head.data;
                head = tail = null;
                currentSize--;
                modCounter++;
                return toReturn;
            }

        Node<E> curr = head;
        Node<E> prev = null;
        while(curr != null) {
            if(curr.data.compareTo(obj) == 0) {
                E temp = curr.data;
                prev.next = curr.next;
                currentSize--;
                modCounter++;
                return temp;
            }
            prev = curr;
            curr = curr.next;
        }
        return null;
    }

    public boolean add(E obj) {
        if(head == null) head = tail = new Node<E>(obj);
        else {
            tail.next = new Node<>(obj);
            tail = tail.next;
        }
        modCounter++;
        currentSize++;
        return true;
    }

    public void clear() {
        head = tail = null;
        currentSize = 0;
        modCounter++;
    }

    public E find(E e) {
        Node<E> curr = head;
        while(curr != null) {
            if(curr.data.compareTo(e) == 0) return curr.data;
            curr = curr.next;
        }
        return null;
    }

    public Iterator<E> iterator() {
        return new Iterator<E>() {
            Node<E> curr = head;
            long modC = modCounter;
            public boolean hasNext() {
                if(curr == null) return false;
                return true;
            }

            public E next() {
                if(modC != modCounter) throw new ConcurrentModificationException("Tainted Iterator!");
                E toReturn = curr.data;
                curr = curr.next;
                return toReturn;
            }
        };
    }

}

class Node<T> {

    T data;
    Node<T> next;

    public Node(T t) {
        this.data = t;
        next = null;
    }

}
