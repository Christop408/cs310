/**
 * Created by chrisgilardi [cssc0922]
 */

package data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class OrderedArrayPriorityQueue<E extends Comparable<E>> implements PriorityQueue<E> {

    E[] underArray;
    private int currentSize;
    private int modificationCounter = 0;

    public OrderedArrayPriorityQueue() {
        this(DEFAULT_MAX_CAPACITY);
    }

    public OrderedArrayPriorityQueue(int maxCapacity) {
        currentSize = 0;
        underArray = (E[]) new Comparable[maxCapacity];
    }

    public boolean insert(E object) {
        //TODO: Find First instance of the priority directly after, insert it before (will put it in order).
        if(!isFull()) {
            if (isEmpty()) {
                underArray[currentSize] = object;
            } else {
                int index = insertSearch(object, 0, currentSize - 1);
                for (int i = currentSize; i > index; i--)
                    underArray[i] = underArray[i - 1];
                underArray[index] = object;
            }
            currentSize++;
            modificationCounter++;
            return true;
        }
        return false;
    }

    public E remove() {
        E temp = peek();
        currentSize--;
        modificationCounter++;
        return temp;
    }

    public E peek() {
        if(currentSize == 0) return null;
        return underArray[currentSize - 1];
    }

    public boolean contains(E obj) {
        return binSearch(obj, 0, currentSize - 1) != -1;
    }

    public int size() {
        return currentSize;
    }

    public void clear() {
        currentSize = 0;
        modificationCounter = 0;
    }

    public boolean isEmpty() {
        return currentSize <= 0;
    }

    public boolean isFull() {
        return currentSize == underArray.length;
    }

    public Iterator iterator() {
        return new Iterator() {
            long modCheck = modificationCounter;
            int iterIndex = 0;

            public boolean hasNext() {
                if(modCheck != modificationCounter) throw new RuntimeException("ERROR: Tainted Iterator");
                if(iterIndex == currentSize - 1) return false;
                return underArray[iterIndex + 1] != null;
            }

            public Object next() {
                if(!hasNext()) throw new NoSuchElementException();
                return underArray[iterIndex++];
            }

            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }

    private int binSearch(E obj, int lo, int hi) {
        if(hi < lo) return -1;
        int mid = (lo + hi) >> 1;
        int status = obj.compareTo(underArray[mid]);
        if(status == 0) return mid;
        if(status > 0) return binSearch(obj, lo, mid - 1);
        return binSearch(obj, mid + 1, hi);
    }

    private int insertSearch(E obj, int lo, int hi) {
        if(hi < lo) return lo;
        int mid = (lo+hi) >> 1;
        if(obj.compareTo(underArray[mid]) >= 0) return insertSearch(obj, lo, mid-1);
        return insertSearch(obj, mid+1, hi);
    }

}
