/**
 * Created by chrisgilardi [cssc0922]
 */

package data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class OrderedList<E extends Comparable<E>> implements Iterable {

    //DON'T HAVE A TAIL POINTER
    Node<E> head;
    int currentSize;
    private int modificationCounter;

    public OrderedList() {
        currentSize = 0;
        head = null;
    }

    public boolean insert(E obj) {
        Node<E> newNode = new Node<E>(obj);
        Node<E> prev = null, curr = head;

        while(curr != null && obj.compareTo(curr.data) >= 0) {
            prev = curr;
            curr = curr.next;
        }
        if(prev == null) {
            newNode.next = head;
            head = newNode;
        } else {
            prev.next = newNode;
            newNode.next = curr;
        }
        currentSize++;
        modificationCounter++;
        return true;
    }

    public Iterator iterator() {
        return new Iterator() {
            Node<E> temp = head;
            long modCheck = modificationCounter;
            public boolean hasNext() {
                if(modCheck != modificationCounter) throw new RuntimeException("ERROR: Tainted Iterator");
                return temp != null;
            }

            public Object next() {
                if(hasNext()) {
                    Node<E> retTemp = temp;
                    temp = temp.next;
                    return retTemp.data;
                }
                throw new NoSuchElementException();
            }
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }

    public boolean isEmpty() {
        return currentSize == 0;
    }

    public E peek() {
        System.out.println(head.data);
        return head.data;
    }

    public int size() {
        return currentSize;
    }

    public boolean contains(E other) {
        if(head == null) return false;
        Node<E> curr = head;
        while(curr != null) {
            if(curr.data.compareTo(other) == 0) return true;
        }
        return false;
    }

    public E remove() {
        if(head != null) {
            Node<E> temp = head;
            head = head.next;
            currentSize--;
            modificationCounter++;
            return temp.data;
        }
        return null;
    }

    public void clear() {
        head = null;
        currentSize = 0;
        modificationCounter = 0;
    }

    class Node<T> {
        T data;
        public Node<T> next;

        public Node(T object) {
            data = object;
            next = null;
        }
    } // End Node class.

}
