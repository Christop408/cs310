/**
 * Created by chrisgilardi [cssc0922]
 */
package data_structures;

import java.util.Iterator;
import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;

public class UnorderedArrayPriorityQueue<E extends Comparable<E>> implements PriorityQueue<E> {

    int currentSize;
    private long modificationCounter;
    E[] underArray;

    public UnorderedArrayPriorityQueue() {
        this(DEFAULT_MAX_CAPACITY);
    }

    public UnorderedArrayPriorityQueue(int maxCapacity) {
        currentSize = 0;
        modificationCounter = 0;
        underArray = (E[]) new Comparable[maxCapacity];
    }

    public boolean insert(E object) {
        if(!isFull()) {
            underArray[currentSize++] = object;
            modificationCounter++;
            return true;
        }
        return false;
    }

    public E remove() {
        if(!isEmpty()) {
            int tempIndex = findLowest();
            E temp = underArray[tempIndex];
            for(int i = tempIndex; i < currentSize - 1 ; i++)
                underArray[i] = underArray[i + 1];
            currentSize--;
            modificationCounter++;
            return temp;
        }
        return null;
    }

    public E peek() {
        if(!isEmpty()) return underArray[findLowest()];
        return null;
    }

    private int findLowest() {
        int lowestIndex = 0;
        for(int i = 1 ; i < currentSize ; i++) {
            if(underArray[lowestIndex].compareTo(underArray[i]) > 0) lowestIndex = i;
        }
        return lowestIndex;
    }

    public boolean contains(E obj) {
        for(int i = 0; i < currentSize ; i++)
            if(underArray[i].compareTo(obj) == 0) return true;
        return false;
    }

    public int size() {
        return currentSize;
    }

    public void clear() {
        currentSize = 0;
    }

    public boolean isEmpty() {
        return currentSize == 0;
    }

    public boolean isFull() {
        return currentSize == underArray.length;
    }

    public Iterator iterator() {
        return new Iterator() {
            int iterIndex = 0;
            long modCheck = modificationCounter;

            public boolean hasNext() {
                if(modCheck != modificationCounter) throw new RuntimeException("ERROR: Tained Iterator");
                if(iterIndex == currentSize - 1) return false;
                return underArray[iterIndex + 1] != null;
            }

            public Object next() {
                if(!hasNext()) throw new NoSuchElementException();
                return underArray[iterIndex++];
            }

            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }
}
