/**
 * Created by chrisgilardi [cssc0922]
 */
package data_structures;

import java.util.Iterator;

public class UnorderedListPriorityQueue<E extends Comparable<E>> implements PriorityQueue<E> {

    private UnorderedList<E> list = new UnorderedList<>();

    public UnorderedListPriorityQueue() {
        list = new UnorderedList<>();
    }

    public boolean insert(E object) {
        list.insertLast(object);
        return true;
    }

    public E remove() {
        return list.removeMin();
    }

    public E peek() {
        return list.findMin();
    }

    public boolean contains(E obj) {
        return list.contains(obj);
    }

    public int size() {
        return list.getSize();
    }

    public void clear() {
        list.clear();
    }

    public boolean isEmpty() {
        return list.isEmpty();
    }

    public boolean isFull() {
        return false;
    }

    public Iterator iterator() {
        return list.iterator();
    }
}
