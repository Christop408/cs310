/**
 * Created by chrisgilardi [cssc0922]
 */
package data_structures;

import java.util.Iterator;

public class OrderedListPriorityQueue<E extends Comparable<E>> implements PriorityQueue<E> {

    OrderedList<E> list = new OrderedList<>();

    public boolean insert(E object) {
        return list.insert(object);
    }

    public E remove() {
        return list.remove();
    }

    public E peek() {
        return list.peek();
    }

    public boolean contains(E obj) {
        return list.contains(obj);
    }

    public int size() {
        return list.size();
    }

    public void clear() {
        list.clear();
    }

    public boolean isEmpty() {
        return list.isEmpty();
    }

    public boolean isFull() {
        return false;
    }

    public Iterator iterator() {
        return list.iterator();
    }
}
