package data_structures;

import java.util.Iterator;

/**
 * Created by Chris on 3/6/2017.
 */
public class LinkedList<E extends Comparable<E>> implements Iterable {

    private Node<E> head;
    private Node<E> tail;
    private int currentSize;
    private long modificationCounter;

    public LinkedList() {
        head = tail = null;
        currentSize = 0;
    }

    public boolean add(E e) {
        if(head == null)
            head = tail = new Node<>(e);
        else {
            tail.next = new Node<>(e);
            tail = tail.next;
        }
        currentSize++;
        modificationCounter++;
        return true;
    }

    public void add(int index, E e) {
        if(index > currentSize || index < 1) throw new IndexOutOfBoundsException();
        else if(head == null || (head != null && index == currentSize)) {
            this.add(e);
            return;
        }
        Node<E> prev = head;
        for(int i = 2; i < index ; i++) {
            prev = prev.next;
        }
        Node<E> temp = new Node<>(e);
        temp.next = prev.next;
        prev.next = temp;
        currentSize++;
        modificationCounter++;
    }

    public void addFirst(E e) {
        if(head == null)
            head = new Node<>(e);
        else {
            Node<E> tempFirst = new Node<>(e);
            tempFirst.next = head;
            head = tempFirst;
            currentSize++;
        }
        modificationCounter++;
    }

    public void addLast(E e) {
        add(e);
    }

    public E peek() {
        if(head == null)
            return null;
        return head.data;
    }

    public E peekLast() {
        if(tail == null)
            return null;
        return tail.data;
    }

    public E get(int index) {
        Node<E> node = getNode(index);
        if(node != null)
            return node.data;
        return null;
    }

    private Node<E> getNode(int index) {
        if(index < 1 || index > currentSize) throw new IndexOutOfBoundsException();
        Node<E> curr = head;
        for(int i = 1 ; i < index; i++) {
            curr = curr.next;
        }
        return curr;
    }

    public E getFirst() {
        return head.data;
    }

    public E getLast() {
        return tail.data;
    }

    public E removeFirst() {
        if(head == null) return null;
        Node<E> temp = head;
        head = head.next;
        return temp.data;
    }

    public E removeLast() {
        if(tail != null) {
            Node<E> curr = head;
            while (curr.next != null) {
                if (curr.next == tail) {
                    Node<E> temp = tail;
                    curr.next = null;
                    tail = curr;
                    modificationCounter++;
                    return temp.data;
                }
                curr = curr.next;
            }
        }
        return null;
    }

    public E remove(int index) {
        int i = 2;
        if(head != null) {
            Node<E> curr = head;
            while(curr.next != null) {
                if(i == index) {
                    Node<E> temp = curr.next;
                    curr.next = temp.next;
                    temp.next = null;
                    return temp.data;
                }
                else {
                    i++;
                    curr = curr.next;
                }
            }
        }
        return null;
    }

    public E set(int index, E element) {
        Node<E> temp = getNode(index);
        getNode(index).data = element;
        modificationCounter++;
        return temp.data;
    }

    public int indexOf(E other) {
        int index = 1;
        Node<E> curr = head;
        while(curr != null) {
            if(other.compareTo(curr.data) == 0) return index;
            curr = curr.next;
            index++;
        }
        return -1;
    }

    public void clear() {
        head = tail = null;
        modificationCounter = 0;
    }

    public boolean contains(E other) {
        if(head != null) {
            Node<E> curr = head;
            while (curr.next != null)
                if (other.compareTo(curr.data) == 0) return true;
        }
        return false;
    }

    public Iterator iterator() {
        return null;
    }

    public String toString() {
        Node<E> curr = head;
        String out = "";
        while(curr != null) {
            out += curr.data.toString() + " ";
            curr = curr.next;
        }
        return out;
    }

    class Node<T> {
        T data;
        Node<T> next;

        public Node(T obj) {
            data = obj;
            next = null;
        }

    }
}
