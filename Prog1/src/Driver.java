/**
 * Created by chrisgilardi on 1/23/17.
 */

import data_structures.ArrayLinearList;
import data_structures.LinearListADT;

public class Driver {


    public Driver() {

    }

    public static void main(String[] args) {
        Driver d = new Driver();
        d.runTests();
    }



    private void runTests() {
        LinearListADT<Integer> list = new ArrayLinearList<Integer>();
        list.insert(1,1);
        list.insert(2,2);
        list.insert(3,2);
        list.insert(4,2);
        list.insert(5,5);
        try {
            list.insert(2,0);  // should throw an exception!
        }
        catch(RuntimeException e) {
            System.out.println("err1");
        }
        try {
            list.insert(77777,7);  // should throw an exception!
        }
        catch(RuntimeException e) {
            System.out.println("err2");
        }

        try {
            list.remove(100);
        }catch(RuntimeException e) {
            //e.printStackTrace();
        }



        list.addFirst(-1);
        list.addLast(-1);
        //Should print -1,1,4,3,2,5,-1
        for(int i : list)
           System.out.println(i);

        list.clear();
        System.out.println("Clearing");
        for(int i : list) // should not print anything, nor crash
            System.out.println(i);

        for(int i=0; i < 100; i++)
            list.insert((i+1),1);
        for(int i : list) // should print 100 .. 1
            System.out.print(i+" ");
        System.out.println();
    }

}
