package data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Created by chrisgilardi on 1/18/17.
 */
public class ArrayLinearList<E> implements LinearListADT<E> {

    private E[] underArray;
    private int currentSize = 0;

    public ArrayLinearList() {
        underArray = (E[]) new Object[DEFAULT_MAX_CAPACITY];
    }

    @Override
    public void addLast(E obj) {
        expandIfNeeded();
        underArray[currentSize++] = obj;
    }

    @Override
    public void addFirst(E obj) {
        insert(obj,1);
    }

    @Override
    public void insert(E obj, int location) {
        if(location > currentSize + 1) throw new ArrayIndexOutOfBoundsException("Index: " + location + ", Size: " +
                currentSize);
        if(location <= 0) throw new ArrayIndexOutOfBoundsException(location);

        expandIfNeeded();

        for(int i = size() ; i >= location ; i--)
            underArray[i] = underArray[i-1];
        underArray[location - 1] = obj;
        currentSize++;


    }

    @Override
    public E remove(int location) {
        E removed;
        try {
           removed = get(location);
        } catch(ArrayIndexOutOfBoundsException a) {
            throw a;
        }
        for (int i = location - 1; i < currentSize - 1; i++)
            underArray[i] = underArray[i + 1];
        currentSize--;
        shrinkIfNeeded();
        return removed;
    }

    @Override
    public E remove(E obj) {
        shrinkIfNeeded();
        int index = this.locate(obj);
        if(index == -1)
            return null;
        return remove(this.locate(obj));
    }

    @Override
    public E removeFirst() {
        if(isEmpty()) throw new RuntimeException("Cannot remove from empty list.");
        shrinkIfNeeded();
        return remove(1);
    }

    @Override
    public E removeLast() {
        if(isEmpty()) throw new RuntimeException("Cannot remove from empty list.");
        shrinkIfNeeded();
        return underArray[--currentSize];
    }

    @Override
    public E get(int location) {
        if(location < 1 || location > currentSize) throw new ArrayIndexOutOfBoundsException("Location " + location +
                " invalid, must be in range [1," + (currentSize + 1) + "]" );
        return underArray[location - 1];
    }

    @Override
    public boolean contains(E obj) {
        return locate(obj) != -1;
    }


    @Override
    public int locate(E obj) {
        for(int i = 0; i < currentSize; i++)
            if(((Comparable<E>) obj).compareTo(underArray[i]) == 0) return i + 1;
        return -1;
    }

    @Override
    public void clear() {
        currentSize = 0;
    }

    @Override
    public boolean isEmpty() {
        return currentSize == 0;
    }

    @Override
    public int size() {
        return currentSize;
    }

    @Override
    public Iterator<E> iterator() {
        return new Iterator<E>() {
            int iterIndex = 0;

            @Override
            public boolean hasNext() {
                return iterIndex < currentSize;
            }

            @Override
            public E next() {
                if(!hasNext()) throw new NoSuchElementException();
                return underArray[iterIndex++];
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }

    //Expands the underlying array to accomodate additions.
    private void expandIfNeeded() {
        if(currentSize + 1 >= underArray.length) {
            E[] newArray = (E[]) new Object[underArray.length * 2];
            for (int i = 0; i < currentSize; i++) newArray[i] = underArray[i];
            underArray = newArray;
        }
    }

    //Shrinks the underlying array to accomodate deletions leaving it at < 25% capacity.
    private void shrinkIfNeeded() {
        if(currentSize < underArray.length/4) {
            E[] newArray = (E[]) new Object[underArray.length / 2];
            for (int i = 0; i < currentSize; i++) newArray[i] = underArray[i];
            underArray = newArray;
        }
    }
}
