package data_structures;

import java.util.Iterator;

/**
 * Created by chrisgilardi on 2/20/17.
 */
public class LinearLinkedList<E extends Comparable<E>> implements LinearListADT<E> {

    private Node<E> head;
    private Node<E> tail;
    private int currentSize;

    public LinearLinkedList() {
        head = tail = null;
        currentSize = 0;
    }

    public void addLast(E obj) {
        if(tail == null) head = tail = new Node<>(obj);
        else {
            tail.next = new Node<>(obj);
            tail = tail.next;
        }
    }

    public void addFirst(E obj) {
        if(head == null) head = tail = new Node<>(obj);
        else {
            Node<E> temp = new Node<>(obj);
            temp.next = head;
            head = temp;
        }
    }

    public void insert(E obj, int location) {
        if(currentSize == 0) {
            head = tail = new Node<>(obj);
        }
        Node<E> node = getNode(location);
        Node<E> temp = new Node<>(obj);
        temp.next = node.next;
        node.next = temp;
    }

    public E remove(int location) {
        return null;
    }

    public E remove(E obj) {
        return null;
    }

    public E removeFirst() {
        return null;
    }

    public E removeLast() {
        return null;
    }

    public E get(int location) {
        return getNode(location).data;
    }

    //TODO: Implement
    private Node<E> getNode(int location) {
        if(location > currentSize + 1) {
            throw new IndexOutOfBoundsException();
        }
        int index = 0;
        Node<E> temp = head;
        while(temp.next != null) {

        }
        return  null;
    }

    public boolean contains(E obj) {
        return false;
    }

    public int locate(E obj) {
        return 0;
    }

    public void clear() {

    }

    public boolean isEmpty() {
        return false;
    }

    public int size() {
        return 0;
    }

    public Iterator<E> iterator() {
        return null;
    }

    class Node<T> {
        T data;
        Node<T> next;

        public Node(T obj) {
            data = obj;
            next = null;
        }
    }
}
